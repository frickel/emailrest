<?php
namespace EmailRest;

use Mail;
use Psr\Http\Message\ServerRequestInterface as Request;

define('EMAIL_REST_HEADER_DEFAULTS', array(
    "contenttype" => "text/plain;charset=UTF-8",
    "subject" => ""
));

define('EMAIL_REST_HEADER_READS', array(
    "contenttype" => "Content-Type",
    "sender" => "Sender",
    "organization" => "Organization",
    "inReplyTo" => "In-Reply-To",
    "contentTransferEncoding" => "Content-Transfer-Encoding",
    "subject" => "Subject",
    // TODO multi
    "references" => "References",
    "from" => "From",
    "to" => "To",
    "cc" => "CC",
    "bcc" => "BCC",
    "replyto" => "Reply-To"
));

class EmailRest
{
    public function send(Request $request, string $driver, array $driverParam)
    {
        $to = $request->getQueryParam("to");
        $text = $request->getQueryParam("text", "");
        
        // find X-Header
        $xHeaders = array();
        foreach ($request->getQueryParams() as $key => $value) {
            if (EmailRest::startsWith($key, "X-") || EmailRest::startsWith($key, "x-")) {
                $xHeaders[$key] = $key;
            }
        }
        
        // collect header
        $header = array();
        $header["MIME-Version"] = "1.0";
        $header["Date"] = date(DATE_RFC822);
        $header["Message-ID"] = "<" . uniqid("emailrest-", true) . uniqid("-", true) . "@" . gethostname() . ">";
        $this->addHeader($header, $request, EMAIL_REST_HEADER_READS);
        $this->addHeader($header, $request, $xHeaders);
        
        // send
        $mail = Mail::factory($driver, $driverParam);
        return $mail->send($to, $header, $text);
    }

    private static function startsWith($haystack, $needle): string
    {
        return (substr($haystack, 0, strlen($needle)) === $needle);
    }

    private function addHeader(&$header, &$request, $arr, $fnValidate = NULL): void
    {
        if (empty($fnValidate)) {
            $fnValidate = function ($x) {
                return $x;
            };
        }
        foreach ($arr as $queryKey => $headerKey) {
            $queryValue = $request->getQueryParam($queryKey);
            if (empty($value) && ! empty(EMAIL_REST_HEADER_DEFAULTS[$queryKey])) {
                $queryValue = EMAIL_REST_HEADER_DEFAULTS[$queryKey];
            }
            $queryValue = $fnValidate($queryValue);
            if (! empty($queryValue)) {
                $header[$headerKey] = $queryValue;
            }
        }
    }
}