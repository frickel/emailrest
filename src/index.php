<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use EmailRest\EmailRest;

require '../vendor/autoload.php';
require 'EmailRest.php';
require 'config.php';

$app = new \Slim\App();
$app->get('/email', function (Request $request, Response $response, array $args) {
    $emailRest = new EmailRest();
    $sent = $emailRest->send($request, EMAIL_REST_SEND_DRIVER, EMAIL_REST_SEND_PARAM);
    if (PEAR::isError($sent)) {
        return $response->withStatus(500, $sent->getMessage())
            ->withHeader("Content-Type", "text/plain;charset=UTF-8");
        // ->getBody()
        // ->write("Result: $sent <br>")
    }
    return $response->withStatus(200);
});
$app->run();
