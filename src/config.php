<?php 

// see https://pear.php.net/package/Mail/docs/latest/Mail/Mail_mail.html

// smtp, sendmail or smtpmx
define('EMAIL_REST_SEND_DRIVER', 'smtp');

define('EMAIL_REST_SEND_PARAM', array(
    "host" => "localhost",
    "port" => 25
    // optinal add credentials
    // Attention: When you add your credentials, restrict page access to prevent them from becoming an email relay.
    
    //,
    //"auth" => "CRAM-MD5",
    //"username" => "username",
    //"password" => "password"
));
