## Page to send email via REST call

You can add following parameter:

Required

 * text => the body text
 * from => From
 * to => To
 
Optional
 
 * contenttype => Content-Type
 * sender => Sender
 * organization => Organization
 * inReplyTo => In-Reply-To
 * contentTransferEncoding => Content-Transfer-Encoding
 * subject => Subject
 * references => References
 * cc => CC
 * bcc => BCC
 * replyto => Reply-To
 * X-... => X-... (all X-Header)
 
### Install

 * Clone
 * Download Composer
 * Install dependencies with composer
   * ```php composer.phar install --no-dev --no-scripts```
 * Add your configuration in file src/config.php
 * Call 

### Update Dependencies

 * ```php composer.phar self-update```
 * ```php composer.phar update --no-dev --no-scripts```
 
### Example Call

At least you need ``from``, ``to`` and ``text`` query parameter.

Replace to parameter with your email address:

```
http://host/src/email?from=tester@example.com&to=target@example.com&text=Test&subject=A%20test%20email
```

### Rewrite Rule

Example Rewrite rule to rewrite URL requests from root to src/ folder

More information see https://www.slimframework.com/docs/v3/start/web-servers.html

```
location / {
	try_files $uri /src/index.php$is_args$args;
}
```

Now you call use URL w/o src/

```
http://host/email?from=tester@example.com&to=target@example.com&text=Test&subject=A%20test%20email
```


